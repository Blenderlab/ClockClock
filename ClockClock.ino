#include <TimeLib.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306.h" // alias for `#include "SSD1306Wire.h"`
#include "fonts.h"
#include <EEPROM.h>
#include "images.h"
#include "OLEDDisplayUi.h"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <Adafruit_NeoPixel.h>

#define led 2 // !! Correspond à D4  cf : https://arduino-projekte.info/wp-content/uploads/2017/03/wemos_d1_mini_pinout.png
Adafruit_NeoPixel strip = Adafruit_NeoPixel(3, led, NEO_GRB + NEO_KHZ800);

char ssid[] = "LaCabane";  //  your network SSID (name)
char pass[] = "123NousIronsAuBois";       // your network password

int HH = 7;
int mm = 30;
int ss = 0;
int Ahh = 7;
bool Ast = true;
int Amm = 30;
long lastupdate = 0;
int NTPupdate = 5000;
int brightness = 0;

#include <WiFiUdp.h>
unsigned int localPort = 2390;
IPAddress timeServerIP;
const char* ntpServerName = "time.nist.gov";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP udp;

ESP8266WebServer server ( 80 );




SSD1306  display(0x3c, D6, D5);

OLEDDisplayUi ui ( &display );

int screenW = 128;
int screenH = 64;
int clockCenterX = screenW / 3;
int clockCenterY = ((screenH) / 2); // top yellow part is 16 px height
int clockRadius = 32;



String twoDigits(int digits) {
  if (digits < 10) {
    String i = '0' + String(digits);
    return i;
  }
  else {
    return String(digits);
  }
}

void clockOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {

}

void analogClockFrame(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  ui.disableIndicator();
  display->setContrast(0);
  // Draw the clock face
  // display->drawCircle(clockCenterX + x, clockCenterY + y, clockRadius);
  display->drawCircle(clockCenterX + x, clockCenterY + y, 2);
  //
  //hour ticks
  for ( int z = 0; z < 360; z = z + 30 ) {
    //Begin at 0° and stop at 360°
    float angle = z ;
    angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
    int x2 = ( clockCenterX + ( sin(angle) * clockRadius ) );
    int y2 = ( clockCenterY - ( cos(angle) * clockRadius ) );
    int x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
    int y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 8 ) ) ) );
    display->drawLine( x2 + x , y2 + y , x3 + x , y3 + y);
  }

  // display second hand
  float angle = second() * 6 ;
  angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
  int x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 5 ) ) ) );
  int y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 5 ) ) ) );
  display->drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y);
  //
  // display minute hand
  angle = minute() * 6 ;
  angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
  x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 4 ) ) ) );
  y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 4 ) ) ) );
  display->drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y);
  //
  // display hour hand
  angle = hour() * 30 + int( ( minute() / 12 ) * 6 )   ;
  angle = ( angle / 57.29577951 ) ; //Convert degrees to radians
  x3 = ( clockCenterX + ( sin(angle) * ( clockRadius - ( clockRadius / 2 ) ) ) );
  y3 = ( clockCenterY - ( cos(angle) * ( clockRadius - ( clockRadius / 2 ) ) ) );
  display->drawLine( clockCenterX + x , clockCenterY + y , x3 + x , y3 + y);


  String timenow = String(hour()) + ":" + twoDigits(minute());
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  //display->setFont(Crushed_Plain_36);
  display->setFont(ArialMT_Plain_24);
  display->drawString( 115 , 0, String(hour())  );
  display->drawString( 115 , 24, String(minute())  );
  display->setFont(ArialMT_Plain_10);
  display->drawString( 115 , 50, String(Ahh) + ":" + String(Amm));
  display->drawXbm(1 , 1, 8, 8, Ast == true ? activeSymbol : inactiveSymbol);

}

void displayIP(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  display->setContrast(0);
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_24);
  display->drawString(clockCenterX + x , 20, String(WiFi.localIP()) );
}

FrameCallback frames[] = { analogClockFrame };

int frameCount = 1;

OverlayCallback overlays[] = { clockOverlay };
int overlaysCount = 1;

void setup() {
  Serial.begin(9600);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  int counter = 0;
  pinMode(led, OUTPUT);

  strip.begin();
  strip.show();
  display.init();
  display.setContrast(0);

  display.clear();
  display.display();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_CENTER);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    display.clear();
    display.drawString(64, 10, "Connexion au WiFi");
    display.drawXbm(46, 30, 8, 8, counter % 3 == 0 ? activeSymbol : inactiveSymbol);
    display.drawXbm(60, 30, 8, 8, counter % 3 == 1 ? activeSymbol : inactiveSymbol);
    display.drawXbm(74, 30, 8, 8, counter % 3 == 2 ? activeSymbol : inactiveSymbol);
    display.display();

    counter++;
  }
  Serial.println(WiFi.localIP());



  udp.begin(localPort);
  ui.setTargetFPS(60);
  ui.setActiveSymbol(activeSymbol);
  ui.setInactiveSymbol(inactiveSymbol);
  ui.setIndicatorPosition(LEFT);
  ui.setIndicatorDirection(LEFT_RIGHT);
  ui.setFrameAnimation(SLIDE_UP);
  ui.setFrames(frames, frameCount);
  ui.setOverlays(overlays, overlaysCount);
  ui.disableAutoTransition();
  ui.init();
  display.flipScreenVertically();

  if (!SPIFFS.begin())
  {
    // Serious problem
    Serial.println("SPIFFS Mount failed");
  } else {
    Serial.println("SPIFFS Mount succesfull");
  }
  server.serveStatic("/", SPIFFS, "/index.html");
  server.begin();
  Serial.println ( "HTTP server started" );
  server.on("/alarm", HTTP_GET, updateAlarm);
  EEPROM.begin(512);
  Ahh = EEPROM.read(0);
  Amm = EEPROM.read(1);
  Ast = EEPROM.read(2);
  updateTime();



}

void updateAlarm() {
  Serial.println("Alaaarm");
  Ahh = (server.arg("hh").toInt());
  Amm = (server.arg("mm").toInt());
  if (server.arg("st") == "on") Ast = true;
  if (server.arg("st") == "off") Ast = false;
  EEPROM.write(0, Ahh);
  EEPROM.write(1, Amm);
  EEPROM.write(2, Ast);
  EEPROM.commit();
  server.send(200);

}
void loop() {
  int remainingTimeBudget = ui.update();
  server.handleClient();
  if (Ahh == hour() && Amm == minute()) {
    uint16_t i;
    
    for (i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel((i + brightness++) & 255));
    }
    strip.show();
  }

  if (millis() - lastupdate > NTPupdate) {
    lastupdate = millis();
    updateTime();
  }


}


void drawProgress(OLEDDisplay *display, int percentage, String label) {
  display->clear();
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  display->drawString(64, 10, label);
  display->drawProgressBar(10, 28, 108, 12, percentage);
  display->display();
}

void updateTime() {
  drawProgress(&display, 50, "Updating Time...");
  WiFi.hostByName(ntpServerName, timeServerIP);
  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  delay(1000);
  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no packet yet");
    drawProgress(&display, 100, "Fail...");
    delay(1000);
    NTPupdate = 5000;

  }
  else {
    Serial.print("packet received, length=");
    Serial.println(cb);
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    const unsigned long seventyYears = 2208988800UL;
    unsigned long epoch = secsSince1900 - seventyYears;

    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    HH = ((epoch  % 86400L) / 3600) + 2; // print the hour (86400 equals secs per day)
    mm = ((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
    ss = (epoch % 60); // print the second
    setTime(HH, mm, ss, 1, 1, 2017);
    drawProgress(&display, 100, "Done !");

    NTPupdate = 3600000;
    delay(1000);
  }

}
unsigned long sendNTPpacket(IPAddress& address)
{
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}


